use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::collections::UnorderedSet;
use near_sdk::{env, near_bindgen, Promise, PromiseOrValue, PanicOnDefault};

static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

pub type AccountId = String;
pub type PublicKey = Vec<u8>;
pub type Salt = u64;

#[near_bindgen]
#[derive(BorshDeserialize, BorshSerialize, PanicOnDefault)]
pub struct Faucet{
    pub account_suffix: AccountId,
    pub min_difficulty: u32,
    pub created_accounts: UnorderedSet<AccountId>
}

fn num_leading_zeros(v: &[u8])-> u32 {
    let mut res = 0;
    for z in v.iter().map(|b| b.leading_zeros()){
        res += z;
        if z < 8{
            break;
        } 
    }
    res
}

fn assert_self(){
    assert_eq!(
        env::current_account_id(),
        env::predecessor_account_id(),
        "Can only be called by owner"
    );
}

#[near_bindgen]
impl Faucet{
    #[init]
    pub fn new(account_suffix: AccountId, min_difficulty: u32) -> Self{
        assert!(env::state_read::<Self>().is_none(), "Already init");
        Self{
            account_suffix,
            min_difficulty,
            created_accounts: UnorderedSet::new(b"a".to_vec())
        }
    }

    pub fn get_account_suffix(&self)->AccountId{
        self.account_suffix.clone()
    }

    pub fn get_min_difficulty(&self)->u32{
        self.min_difficulty
    }

    pub fn get_num_created_accounts(&self)->u64{
        self.created_accounts.len()
    }

    pub fn create_account(&mut self, account_id: AccountId, public_key: PublicKey, salt: Salt) -> PromiseOrValue<()>{
        assert!(account_id.ends_with(&self.account_suffix), "Account has to end with the suffix");

        assert!(!self.created_accounts.contains(&account_id), "The given account is already created");

        let mut message = account_id.as_bytes().to_vec();
        message.push(b':');
        message.extend_from_slice(&public_key);
        message.push(b':');
        message.extend_from_slice(&salt.to_le_bytes());

        let hash = env::sha256(&message);

        assert!(num_leading_zeros(&hash) >= self.min_difficulty , "The proof is work is too weak");

        self.created_accounts.insert(&account_id);

        Promise::new(account_id)
        .create_account()
        .transfer(env::account_balance()/1000)
        .add_full_access_key(public_key)
        .into()

    }

    pub fn set_min_difficulty( &mut self ,min_difficulty: u32){
        assert_self();
        self.min_difficulty = min_difficulty;
    }

    pub fn add_access_key(public_key: PublicKey)-> PromiseOrValue<()>{
        assert_self();
        Promise::new(
            env::current_account_id())
            .add_access_key(
                public_key,
                0, 
                env::current_account_id(), 
                b"create account".to_vec()
            ).into()
    }
}

#[cfg(test)]
mod tests{
    use near_sdk::MockedBlockchain;
    use near_sdk::{testing_env, VMContext};
    use std::panic;

    use super::*;

    fn catch_unwind_silent<F: FnOnce() -> R + panic::UnwindSafe, R > (f:F)->
        std::thread::Result<R>{

            let prev_hook = panic::take_hook();
            panic::set_hook(Box::new(|_|{}));
            let result = panic::catch_unwind(f);
            panic::set_hook(prev_hook);

            result
    }

    fn get_context() -> VMContext {
        VMContext {
            current_account_id: "alice".to_string(),
            signer_account_id: "bob".to_string(),
            signer_account_pk: vec![0, 1, 2],
            predecessor_account_id: "bob".to_string(),
            input: vec![],
            block_index: 0,
            block_timestamp: 0,
            account_balance: 0,
            account_locked_balance: 0,
            storage_usage: 10u64.pow(6),
            attached_deposit: 0,
            prepaid_gas: 10u64.pow(15),
            random_seed: vec![0, 1, 2],
            is_view: false,
            output_data_receivers: vec![],
            epoch_height: 0,
        }
    }

    #[test]
    fn test_new(){
        let context = get_context();
        testing_env!(context);
        let account_suffix = ".alice".to_string();
        let min_difficulty = 5;

        let contract = Faucet::new(account_suffix.clone(), min_difficulty);
        println!("acc suffix {}",contract.get_account_suffix());
        println!("mint difficul {}",contract.get_min_difficulty());
        println!("numcreate Account {}",contract.get_num_created_accounts());
    }

    #[test]
    fn test_create_account_ok(){
        let context = get_context();
        testing_env!(context);
        let account_suffix = ".alice".to_string();
        let min_difficulty = 20;
        let mut contract = Faucet::new(account_suffix.clone(), min_difficulty);
        let account_id = "test.alice";
        let public_key = vec![0u8;33];
        let salt = 89949;

        contract.create_account(account_id.to_string(), public_key, salt);
        println!("numcreate Account2 {}",contract.get_num_created_accounts());
    }

    #[test]
    fn test_fail_default(){
        let context = get_context();
        testing_env!(context);
        catch_unwind_silent(||{
            Faucet::default();
        })
        .unwrap_err();
    }

    #[test]
    fn test_fail_create_account_bad_name(){
        let context = get_context();
        testing_env!(context);
        let account_suffix = ".alice".to_string();
        let min_difficulty = 0;
        let mut contract = Faucet::new(account_suffix.clone(), min_difficulty);
        let account_id = "bob";
        let public_key = vec![0u8;33];
        let salt = 0;
        catch_unwind_silent(move || {
            contract.create_account(account_id.to_string(), public_key, salt);
            println!("numcreate Account3 {}",contract.get_num_created_accounts());
        })
        .unwrap_err();
    }

    #[test]
    fn test_fail_create_account_already_created(){
        let context = get_context();
        testing_env!(context);
        let account_suffix = ".alice".to_string();
        let min_difficulty = 10;
        let mut contract = Faucet::new(account_suffix.clone(), min_difficulty);
        let account_id = "test.alice";
        let public_key = vec![0u8;33];
        let salt = 123;
        contract.create_account(account_id.to_string(), public_key.clone(), salt);

        catch_unwind_silent(move || {
            contract.create_account(account_id.to_string(), public_key, salt);
        })
        .unwrap_err();
    }

}